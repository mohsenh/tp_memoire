/* On inclut l'interface publique */
#include "mem.h"

#include <assert.h>
#include <stddef.h>
#include <string.h>

/* Définition de l'alignement recherché
 * Avec gcc, on peut utiliser __BIGGEST_ALIGNMENT__
 * sinon, on utilise 16 qui conviendra aux plateformes qu'on cible
 */
#ifdef __BIGGEST_ALIGNMENT__
#define ALIGNMENT __BIGGEST_ALIGNMENT__
#else
#define ALIGNMENT 16
#endif

/* structure placée au début de la zone de l'allocateur

   Elle contient toutes les variables globales nécessaires au
   fonctionnement de l'allocateur

   Elle peut bien évidemment être complétée
*/
struct allocator_header {
        size_t memory_size;
	mem_fit_function_t *fit;
	struct fb * tete_libre;
};

/* La seule variable globale autorisée
 * On trouve à cette adresse le début de la zone à gérer
 * (et une structure 'struct allocator_header)
 */
static void* memory_addr;

static inline void *get_system_memory_addr() {
	return memory_addr;
}

static inline struct allocator_header *get_header() {
	struct allocator_header *h;
	h = get_system_memory_addr();
	return h;
}

static inline size_t get_system_memory_size() {
	return get_header()->memory_size;
}


struct fb {
	size_t size;
	struct fb* next;
	/* ... */
};


void mem_init(void* mem, size_t taille)
{
        memory_addr = mem;
        *(size_t*)memory_addr = taille;
	/* On vérifie qu'on a bien enregistré les infos et qu'on
	 * sera capable de les récupérer par la suite
	 */
	assert(mem == get_system_memory_addr());
	assert(taille == get_system_memory_size());
	/* ... */
	
	mem_fit(&mem_fit_first);
	get_header()->tete_libre=memory_addr+sizeof(struct allocator_header);
	get_header()->tete_libre->size=taille-sizeof(struct allocator_header)-sizeof(struct fb);
	get_header()->tete_libre->next=NULL;
}

void mem_show(void (*print)(void *, size_t, int)) {
	/* ... */
	void *compare=get_system_memory_addr()+get_system_memory_size();;
	void * pointeur=memory_addr+sizeof(struct allocator_header);
	struct fb * pointeur_libre=get_header()->tete_libre;
	if (pointeur_libre==NULL)
		{
		while(compare> (void*)pointeur)
			{ 
			if (*(size_t*)pointeur!=0)
					{
					print(pointeur, *(size_t*)pointeur , 0);
					}
			pointeur=pointeur+sizeof(size_t)+*(size_t*)pointeur;
			}
		}
	while (pointeur_libre!= NULL) {
		if (pointeur != (void*) pointeur_libre)
			{
			while (pointeur != (void*) pointeur_libre)
				{
				print(pointeur, *(size_t*)pointeur , 0);
				pointeur=pointeur+sizeof(size_t)+*(size_t*)pointeur;
				}
			}
		print((void*) pointeur_libre, pointeur_libre->size, 1);
		pointeur=pointeur+sizeof(struct fb)+pointeur_libre->size;	
		pointeur_libre=pointeur_libre->next;
		if (pointeur_libre==NULL && compare> (void*)pointeur)
			{
			while(compare> (void*)pointeur)
				{ 
				if (*(size_t*)pointeur!=0)
						{
						print(pointeur, *(size_t*)pointeur , 0);
						}
				pointeur=pointeur+sizeof(size_t)+*(size_t*)pointeur;
				}
			}
	}
}

void mem_fit(mem_fit_function_t *f) {
	get_header()->fit = f;
}

void *mem_alloc(size_t taille) {
	/* ... */
	__attribute__((unused)) /* juste pour que gcc compile ce squelette avec -Werror */
	struct fb *fb = get_header()->fit(get_header()->tete_libre, taille);
	/* ... */
	return fb;
}

void mem_free(void* mem) {

  struct fb * temp=NULL;
  void * pointeur = memory_addr + sizeof(struct allocator_header);

  struct fb * pointeur_libre = get_header()->tete_libre;
  
  size_t taille_pointeur_libre ;
  size_t taille_mem = *(size_t*)mem + sizeof(size_t) ;
  void * pointeur_fin_mem = mem + *(size_t*)mem + sizeof(size_t);

  struct fb * dernier_pointeur_libre= get_system_memory_addr() + get_system_memory_size();
  struct fb * dernier_pointeur_libre_next;
  size_t dernier_pointeur_libre_size;
  
  size_t temp_size_t=0;
  size_t taille_a_ramasser=0;
  
  
  // Le cas ou on aura a la fin de notre memoire, de la memoire perdue, c'est-a-dire si il nous restera a la fin une memoire de taille <= 16
  if (mem != NULL)
  	{
  if ((void*)dernier_pointeur_libre-(mem+(int)taille_mem) <= 16){
  
      taille_a_ramasser = (void *)dernier_pointeur_libre - (mem + taille_mem);
      
   // Le cas ou on a encore de zones libres dans la memoire
   if (pointeur_libre != NULL){ 
  
   //struct fb * pointeur_libre_next = get_header()->tete_libre->next;
   //taille_pointeur_libre = get_header()->tete_libre->size;
   
  
      
            
            // On recupere la derniere zone libre qui est juste avant la zone occupee mem
            while (pointeur_libre != NULL && (void*)pointeur_libre < mem){
                 dernier_pointeur_libre = pointeur_libre;
                 pointeur_libre = pointeur_libre->next;
            }

            dernier_pointeur_libre_size = dernier_pointeur_libre->size;
            

            
            // Si on a avant la zone occupee mem une zone libre
            if ( (void*)dernier_pointeur_libre+dernier_pointeur_libre_size+sizeof(struct fb) == mem){
                 dernier_pointeur_libre->size = dernier_pointeur_libre_size + taille_mem + taille_a_ramasser;
            }

            // Si on a avant la zone occupee mem une zone occupee
            if (((void*)dernier_pointeur_libre+dernier_pointeur_libre_size+sizeof(struct fb)) != mem){
                 struct fb * nouveau_fb = mem;
                 nouveau_fb->size = taille_mem - sizeof(struct fb) + taille_a_ramasser;
                 nouveau_fb->next = NULL;
                 dernier_pointeur_libre->next = nouveau_fb;
            }
      
   
  }
  
  
  // Le cas ou on n'a plus de zones libres dans la memoire, et donc toute la memoire est occupee
  else{
       get_header()->tete_libre = (struct fb*) mem;
       get_header()->tete_libre->size = taille_mem - sizeof(struct fb) + taille_a_ramasser;
       get_header()->tete_libre->next = NULL;
  }
      
  }
  
  
  
  // Le cas ou on n'aura pas a la fin de notre memoire, de la memoire perdue, c'est-a-dire si il nous restera a la fin une memoire de taille > 16
  
  else{
  
 if (pointeur_libre != NULL){ 
  
   struct fb * pointeur_libre_next = get_header()->tete_libre->next;
 taille_pointeur_libre = get_header()->tete_libre->size;
   // Le cas ou la zone occupee mem est tout au debut de la memoire
   if (mem == pointeur){
  
       // Si la zone qui suit directement la zone occupee mem est une zone libre
       if (pointeur_fin_mem == (void*) pointeur_libre){
            get_header()->tete_libre = mem;
	    get_header()->tete_libre->size = taille_pointeur_libre + taille_mem;
	    get_header()->tete_libre->next = pointeur_libre_next;
       }
       
       // Si la zone qui suit directement la zone occupee mem est une zone occupee 
       else{
    	    temp=get_header()->tete_libre;
     	    get_header()->tete_libre = mem;
	    get_header()->tete_libre->size = taille_mem - sizeof(struct fb);
	    get_header()->tete_libre->next = temp;
       }
   }

   // Le cas ou la zone occupee mem n'est pas au debut de la memoire
   else{
  
       // Si il n'y a pas aucune zone libre avant la zone occupee mem
       if ((void*)pointeur_libre > mem){
       
           // Si la zone qui suit directement la zone occupee mem est une zone libre
           if (pointeur_fin_mem == (void*) pointeur_libre){
                get_header()->tete_libre = mem;
    	        get_header()->tete_libre->size = taille_pointeur_libre + taille_mem;
    	        get_header()->tete_libre->next = pointeur_libre_next;
           }
           
           // Si la zone qui suit directement la zone occupee mem est une zone occupee 
           else{
               get_header()->tete_libre = mem;
    	       get_header()->tete_libre->size = taille_mem - sizeof(struct fb);
    	       get_header()->tete_libre->next = pointeur_libre;
           }
       }

       // Si il existe au moins une zone libre avant la zone occupee mem
       else{
            
            // On recupere la derniere zone libre qui est juste avant la zone occupee mem
            while (pointeur_libre != NULL && (void*)pointeur_libre < mem){
                 dernier_pointeur_libre = pointeur_libre;
                 pointeur_libre = pointeur_libre->next;
            }

            dernier_pointeur_libre_size = dernier_pointeur_libre->size;
            dernier_pointeur_libre_next = pointeur_libre;

            // Si on a avant et apres la zone occupee mem des zones libres
            if (((void*)dernier_pointeur_libre+dernier_pointeur_libre_size+sizeof(struct fb)) == mem && pointeur_fin_mem == (void*)dernier_pointeur_libre_next){
                 dernier_pointeur_libre->size = dernier_pointeur_libre_size + taille_mem + (dernier_pointeur_libre_next->size) + sizeof(struct fb);
                 dernier_pointeur_libre->next = dernier_pointeur_libre_next->next;
            }

            // Si on a avant et apres la zone occupee mem des zones occupees
            if (((void*)dernier_pointeur_libre+dernier_pointeur_libre_size+sizeof(struct fb)) != mem && pointeur_fin_mem != (void*)dernier_pointeur_libre_next){
                 struct fb * new_fb = mem;
                 new_fb->size = taille_mem - sizeof(struct fb);
                 new_fb->next = dernier_pointeur_libre_next;
                 dernier_pointeur_libre->next = new_fb;
            }

            // Si on a avant la zone occupee mem une zone libre, et apres une zone occupee
            if ( (void*)dernier_pointeur_libre+dernier_pointeur_libre_size+sizeof(struct fb) == mem && pointeur_fin_mem != (void*)dernier_pointeur_libre_next){
                 dernier_pointeur_libre->size = dernier_pointeur_libre_size + taille_mem;
            }

            // Si on a avant la zone occupee mem une zone occupee, et apres une zone libre
            if (((void*)dernier_pointeur_libre+dernier_pointeur_libre_size+sizeof(struct fb)) != mem && pointeur_fin_mem == (void*)dernier_pointeur_libre_next){
                 temp_size_t = dernier_pointeur_libre_next->size;
                 dernier_pointeur_libre_next = (struct fb*) mem;
                 dernier_pointeur_libre_next->size = temp_size_t+ taille_mem;
                 dernier_pointeur_libre->next = dernier_pointeur_libre_next;
            }
      }
   }
  }
  
  
  // Le cas ou on n'a plus de zones libres dans la memoire, et donc toute la memoire est occupee
  else{
       get_header()->tete_libre = (struct fb*) mem;
       get_header()->tete_libre->size = taille_mem - sizeof(struct fb);
       get_header()->tete_libre->next = NULL;
  }

}
}
}	



struct fb* mem_fit_first(struct fb *list, size_t size) {
	struct fb * pointeur= list;
	void * besoin = NULL;
	void * temp = NULL;
	
	// la variable compare nou
	void *compare;
	struct fb* Prec;
	struct fb* P;
	size_t taille;
	size_t temp_size;
	
	//une variable utiliser comme booléen, si trouver=1 alors on a trouvé une zone libre qui correspond sion on 	a pas trouvé
	int trouver=0;
	
	//si l'utilisateur entre une valeur negative on refuse l'allocation 
	if ((int)size>0)
	{
	
	//d'abord on vérifie si on a bien des zones libres dans notre zone mémoire
	if (pointeur!= NULL)
		{
		
		Prec=list;
		P=list->next;
		
		//Si la 1èr case libre correspond donc on devra modifier la tete de la liste des zones libres
		//le + 8 correspond a la taille du pointeur qui pourra etre consideré comme zone libre
		if (pointeur->size+8 >= size)
			{
			
			//on a trouvé une case qui correspond alors trouver=vrai
			trouver=1;
			taille=get_header()->tete_libre->size-size-sizeof(size_t);
			besoin=list;
			
			//si on va allouer la taille maximal c'est ça dire on partitionne pas 
			if (pointeur->size+8==size)
				{
				besoin=get_header()->tete_libre;
				get_header()->tete_libre=get_header()->tete_libre->next;				
				*(size_t*)besoin=size;
				}
				
			// si on alloue une taille inférieur de la taille maximal	
			else
				{
				compare=get_system_memory_addr()+get_system_memory_size();
				
				//si on fait pas un dépassement de mémoir 
				if(compare>(void*)besoin+sizeof(size_t)+size*sizeof(void))
					{
					
					//on partitionne si on s'assure qu'on une taille restante strictement superieur à 16    
					if ((int)taille>0)
						{
						//on décale la tete
						get_header()->tete_libre=(void*)besoin+sizeof(size_t)+size*sizeof(void); 
						//on utilise la variable temp pour sauvegarde l'information 
						temp=((struct fb *)besoin)->next;
						
						//la nouvelle case libre partionné aura cette "taille"
						get_header()->tete_libre->size=taille;
						get_header()->tete_libre->next=temp;
						
						//on mémorise la taille de la zone ocupée au tout debut
						*(size_t*)besoin=size;
						}
						
					//on partionne pas car la taille restante est inférieur ou égale à 16
					//ici on procède a une majoration pour éviter de la zone mémoire perdue
					else
						{
						//temp_size reçoit la taille qu'on pourrait perdre 
						temp_size=pointeur->size-size+8;
						besoin=get_header()->tete_libre;
						get_header()->tete_libre=get_header()->tete_libre->next;				
						//majoration 
						*(size_t*)besoin=size+temp_size;
						}
					}
					
				// si on pourrait faire un dépassement de mémoire alors on fait pas de partionnement 
				else
					{
					besoin=get_header()->tete_libre;
					get_header()->tete_libre=get_header()->tete_libre->next;				
					*(size_t*)besoin=size;
					}
				 }
			}

		//la case mémoire qui pourrait correspondre n'est pas au debut donc on parcours la liste
		else
			{
			
			//pou la supression on aure besoin de deux pointeur 
			Prec=list;
			P=list->next;	
			
			//parcours de la liste jusqu'a trouver une case qui correspond 
			while (P!= NULL && trouver==0 )
				{
				if (P->size+8 >= size)
			 		{
			 		trouver=1;
			 		}
				else
			 		{
			 		Prec=P;
			 		P=P->next;
			 		}
				}
				
			//si on a effictivement trouvé une case qui correspond 
			//puis on procède au méme raisonnement qu'au avec quelques modifications 
			if (trouver==1)
				{
				taille=P->size-size-sizeof(size_t);
				besoin=P;
			
				if (P->size+8==size)
					{
					Prec->next=P->next;
					*(size_t*)besoin=size;
					}
				else
					{
					compare=get_system_memory_addr()+get_system_memory_size();
					if(compare>(void*)besoin+sizeof(size_t)+size*sizeof(void))
						{
						if ((int) taille >0)
							{
							Prec->next=(void*)besoin+sizeof(size_t)+size*sizeof(void);
							temp=((struct fb *)besoin)->next;
							P=Prec->next; 
							P->size=taille;
							P->next=temp;
							*(size_t*)besoin=size;
							}
						else
							{
							temp_size=P->size-size+8;
							Prec->next=P->next;
							*(size_t*)besoin=size+temp_size;
							}
						}
					else
						{
						Prec->next=P->next;
						*(size_t*)besoin=size;
						}
					
					}
				}	
			}
		}
	}
	
	//si on a pas trouvé on retourne NULL
	if (trouver==0)
		{
		return NULL;
		}
	//sinon on retorune l'adresse du pointeur alloué
	else
		{
		return besoin+sizeof(size_t);
		}
	 
}

/* Fonction à faire dans un second temps
 * - utilisée par realloc() dans malloc_stub.c
 * - nécessaire pour remplacer l'allocateur de la libc
 * - donc nécessaire pour 'make test_ls'
 * Lire malloc_stub.c pour comprendre son utilisation
 * (ou en discuter avec l'enseignant)
 */
size_t mem_get_size(void *zone) {
	/* zone est une adresse qui a été retournée par mem_alloc() */

	/* la valeur retournée doit être la taille maximale que
	 * l'utilisateur peut utiliser dans cette zone */
	return *(size_t*)(zone-sizeof(size_t));
}

/* Fonctions facultatives
 * autres stratégies d'allocation
 */
struct fb* mem_fit_best(struct fb *list, size_t size) {
	return NULL;
}

struct fb* mem_fit_worst(struct fb *list, size_t size) {
	return NULL;
}
